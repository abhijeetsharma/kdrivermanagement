# Kdrivermanagmenet
Solution For Driver management

## Introduction
Kdrivermanagmenet is a KDED module to listen for new devices and able to suggest suitable driver on the go. The Kdrivermanagmenet uses [linux-driver-management](https://github.com/solus-project/linux-driver-management) for device enumeration, hotplug capablities and device abstraction. Intial Proposal can be found [here](Discover_ldm-integration.pdf)

